var tab1 = [
	["A", "B", "C"],
	[ 1,   2,   3 ],
	[true, false, false]
];
/*
devient :

var tab2 = [
	[true,  1, "A"],
	[false, 2, "B"],
	[false, 3, "C"]
];
*/

var tab2 = []

tab1 = tab1.reverse()

for (var i = 0; i < tab1.length; i++) {
    tab2.push([]);
  for (var j = 0; j < tab1.length; j++) {
   tab2[i].push(tab1[j][i]);
  }

}
console.log(tab2);
